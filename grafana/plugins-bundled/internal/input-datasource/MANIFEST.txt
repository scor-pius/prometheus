
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

{
  "manifestVersion": "2.0.0",
  "signatureType": "grafana",
  "signedByOrg": "grafana",
  "signedByOrgName": "Grafana Labs",
  "plugin": "input",
  "version": "1.0.0",
  "time": 1690314989670,
  "keyId": "7e4d0c6a708866e7",
  "files": {
    "README.md": "64725ebd88f7505cca78f3204eb9bc58371fc7e84172d4f35e3b7eddc4e18cee",
    "img/input.svg": "8095517d5565d37438bf457e4a2fe5b850a6a6d2d2ecb5378a981828fac58367",
    "module.js": "6d7f7c011a52ff1cb3fe0df967f73b7dcce1669dc1f3c2290397d028574bacd6",
    "module.js.map": "16668fce5aa260d12cc50588b3ec1cd38dd04fcf3a84501a8ce88948c1ecd309",
    "plugin.json": "cf26a3afb7c10cd9ae40b5296d04172b5dac927d69a51082e6d085b34341ccc3"
  }
}
-----BEGIN PGP SIGNATURE-----
Version: OpenPGP.js v4.10.10
Comment: https://openpgpjs.org

wrcEARMKAAYFAmTAKO0AIQkQfk0ManCIZucWIQTzOyW2kQdOhGNlcPN+TQxq
cIhm58/yAgYirnafKtrpQAwQMlsk4zVrUcztym3hsszvx47sM0x0LQRVzcyN
GS2maKdD3UBvsuOIRfVY+s/sC6GPvcTX/5S10AII8x6RIuN1xMdYYAFunOJu
BaDykNR5DVIpQb419mXT1SP239epQBwS6S1MIzQaptIPL20Wf6r2+I2KWTRz
W1qAXhM=
=WXM6
-----END PGP SIGNATURE-----
