export type ValidationResult = {
  valid: boolean;
  errors?: string[];
};
