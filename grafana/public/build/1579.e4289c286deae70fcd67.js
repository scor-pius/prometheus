"use strict";(self.webpackChunkgrafana=self.webpackChunkgrafana||[]).push([[1579],{91579:(Te,ee,i)=>{i.d(ee,{D:()=>Ot});var l=i(9892),t=i(66111),te=i(52666),ae=i(75849),B=i(206),P=i(68183),x=i(86647),C=i(37932),b=i(19426),d=i(31403),ne=i(52081),I=i(68226),M=i(35029),K=i(7804),se=i(59679),G=i(2631),j=i(54899),Oe=i(61744);function Ae({pluginId:e}){const{value:a,loading:n,error:s}=(0,se.Z)(async()=>(0,j.i)().get(`/api/plugins/${e}/markdown/query_help`),[]),r=(0,G.a)(a);return n?t.createElement(Oe.u,{text:"Loading help..."}):s?t.createElement("h3",null,"An error occurred when loading help."):a===""?t.createElement("h3",null,"No query help could be found."):t.createElement("div",{className:"markdown-html",dangerouslySetInnerHTML:{__html:r}})}var k=i(47694),Re=i(29930),Z=i(99822),Pe=i(82897),w=i(41818),ke=i(35645),O=i(72648),A=i(46967),re=i(39904),he=i(30784),fe=i(32817),Me=i(81095),$e=i(23934);const Le="grafana.features.datasources.components.picker.DataSourceDropDown.history";function Qe(){const[e=[],a]=(0,$e.Z)(Le,[]),n=(0,t.useCallback)(s=>{s.meta.builtIn||(e.includes(s.uid)?(e.splice(e.findIndex(r=>s.uid===r),1),a([...e,s.uid])):a([...e,s.uid].slice(1,6)))},[e,a]);return[e,n]}function pe(e){return(0,x.F)().getList(e)}function oe(e){const a=(0,x.F)();return a.getInstanceSettings(e)}function Fe(e){const{keyboardEvents:a,containerRef:n}=e,s=(0,t.useRef)(0),r="data-role",o="keyboardSelectableItem",c={[r]:o},u=`[${r}="${o}"`,f="data-selectedItem",g=`[${f}="true"]`,v=(0,t.useCallback)(m=>{const h=n?.current?.querySelectorAll(u),S=h?.item(m%h?.length);h?.forEach(E=>E.setAttribute(f,"false")),S&&(S.scrollIntoView({block:"center"}),S.setAttribute(f,"true"))},[n,u]),y=(0,t.useCallback)(()=>{n?.current?.querySelector(g)?.querySelector("button")?.click()},[n,g]);return(0,t.useEffect)(()=>{if(!a)return;const m=a.subscribe({next:h=>{switch(h?.code){case"ArrowDown":{v(++s.current),h.preventDefault();break}case"ArrowUp":s.current=s.current>0?s.current-1:s.current,v(s.current),h.preventDefault();break;case"Enter":y();break}}});return()=>m.unsubscribe()},[a,v,y]),(0,t.useEffect)(()=>{const m=new MutationObserver(h=>{h.some(E=>E.addedNodes&&E.addedNodes.length>0||E.removedNodes&&E.removedNodes.length>0)&&v(0)});return n.current&&m.observe(n.current,{childList:!0}),()=>{m.disconnect()}},[n,u,v]),[c,g]}var Be=i(77582),We=i(86475),He=i(3597),Ve=i(27876);function ge({variant:e,onClick:a}){const n=Be.Vt.hasPermission(Ve.AccessControlAction.DataSourcesCreate),s=k.vc.featureToggles.dataConnectionsConsole?We.Z.DataSourcesNew:He.n.New;return t.createElement(d.Qj,{variant:e||"primary",href:s,disabled:!n,tooltip:n?void 0:"You do not have permission to configure new data sources",onClick:a,target:"_blank"},"Configure a new data source")}var ie=i(72948),Ue=i(34807);function ve({ds:e,onClick:a,selected:n,description:s,...r}){const o=(0,O.wW)(ze);return t.createElement(ie.Z,{key:e.uid,onClick:a,className:(0,l.cx)(o.card,n?o.selected:void 0),...r},t.createElement(ie.Z.Heading,{className:o.heading},t.createElement("div",{className:o.headingContent},t.createElement("span",{className:o.name},e.name," ",e.isDefault?t.createElement(Ue.P,{tags:["default"]}):null),t.createElement("small",{className:o.type},s||e.meta.name))),t.createElement(ie.Z.Figure,{className:o.logo},t.createElement("img",{src:e.meta.info.logos.small,alt:`${e.meta.name} Logo`})))}function ze(e){return{card:l.css`
      cursor: pointer;
      background-color: ${e.colors.background.primary};
      border-bottom: 1px solid ${e.colors.border.weak};
      // Move to list component
      margin-bottom: 0;
      border-radius: 0;
      padding: ${e.spacing(1)};
    `,heading:l.css`
      width: 100%;
      overflow: hidden;
      // This is needed to enable ellipsis when text overlfows
      > button {
        width: 100%;
      }
    `,headingContent:l.css`
      color: ${e.colors.text.secondary};
      width: 100%;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      display: flex;
      justify-content: space-between;
    `,logo:l.css`
      width: 32px;
      height: 32px;
      padding: ${e.spacing(0,1)};
      display: flex;
      align-items: center;

      > img {
        max-height: 100%;
        min-width: 24px;
      }
    `,name:l.css`
      color: ${e.colors.text.primary};
      display: flex;
      gap: ${e.spacing(2)};
    `,type:l.css`
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      display: flex;
      align-items: center;
    `,separator:l.css`
      margin: 0 ${e.spacing(1)};
      color: ${e.colors.border.weak};
    `,selected:l.css`
      background-color: ${e.colors.background.secondary};
    `,meta:l.css`
      display: block;
      overflow-wrap: unset;
      white-space: nowrap;
      width: 100%;
      overflow: hidden;
      text-overflow: ellipsis;
    `}}function Y(e,a){return!e||!a?!1:typeof a=="string"?e.uid===a:e.uid===a.uid}function Ke(e){if(e){if(typeof e=="string")return`${e} - not found`;if("name"in e)return e.name;if(e.uid)return`${e.uid} - not found`}}function Ge(e,a,n){return(r,o)=>{if(e&&Y(r,e))return-1;if(e&&Y(o,e))return 1;const c=a.indexOf(r.uid),u=a.indexOf(o.uid);if(c>-1&&c>u)return-1;if(u>-1&&u>c)return 1;const f=n.includes(r.uid),g=n.includes(o.uid);return f&&!g?-1:g&&!f?1:g&&f?r.name<o.name?-1:1:r.meta.builtIn&&!o.meta.builtIn?1:o.meta.builtIn&&!r.meta.builtIn?-1:(r.meta.builtIn&&o.meta.builtIn,r.name<o.name?-1:1)}}function Se(e,a=""){return e.name.toLowerCase().includes(a.toLowerCase())}const je={grafana:"Discover visualizations using mock data","-- Mixed --":"Use multiple data sources","-- Dashboard --":"Reuse query results from other visualizations"};function Ze({className:e,current:a,onChange:n,tracing:s,dashboard:r,mixed:o,metrics:c,type:u,annotations:f,variables:g,alerting:v,pluginId:y,logs:m,filter:h}){const E=pe({tracing:s,dashboard:r,mixed:o,metrics:c,type:u,annotations:f,variables:g,alerting:v,pluginId:y,logs:m}).filter(p=>(h?h?.(p):!0)&&!!p.meta.builtIn);return t.createElement("div",{className:e,"data-testid":"built-in-data-sources-list"},E.map(p=>t.createElement(ve,{key:p.uid,ds:p,description:je[p.uid],selected:Y(p,a),onClick:()=>n(p)})))}var Ye=i(20308);function Ee(e){const a=(0,t.useRef)(null),[n,s]=Fe({keyboardEvents:e.keyboardEvents,containerRef:a}),r=(0,O.l4)(),o=_e(r,s),{className:c,current:u,onChange:f,enableKeyboardNavigation:g,onClickEmptyStateCTA:v}=e,y=pe({alerting:e.alerting,annotations:e.annotations,dashboard:e.dashboard,logs:e.logs,metrics:e.metrics,mixed:e.mixed,pluginId:e.pluginId,tracing:e.tracing,type:e.type,variables:e.variables}),[m,h]=Qe(),S=e.filter?y.filter(e.filter):y;return t.createElement("div",{ref:a,className:(0,l.cx)(c,o.container),"data-testid":"data-sources-list"},S.length===0&&t.createElement(Je,{className:o.emptyState,onClickCTA:v}),S.sort(Ge(u,m,qe())).map(E=>t.createElement(ve,{"data-testid":"data-source-card",key:E.uid,ds:E,onClick:()=>{h(E),f(E)},selected:Y(E,u),...g?n:{}})))}function Je({className:e,onClickCTA:a}){const n=(0,O.wW)(Xe);return t.createElement("div",{className:(0,l.cx)(e,n.container)},t.createElement("p",{className:n.message},"No data sources found"),t.createElement(ge,{onClick:a}))}function Xe(e){return{container:l.css`
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    `,message:l.css`
      margin-bottom: ${e.spacing(3)};
    `}}function qe(){return(0,Ye.J)().getVariables().filter(a=>a.type==="datasource").map(a=>`\${${a.id}}`)}function _e(e,a){return{container:l.css`
      display: flex;
      flex-direction: column;
      ${a} {
        background-color: ${e.colors.background.secondary};
      }
    `,emptyState:l.css`
      height: 100%;
      flex: 1;
    `}}const $="dashboards_dspickermodal_clicked",W={SELECT_DS:"select_ds",UPLOAD_FILE:"upload_file",CONFIG_NEW_DS:"config_new_ds",CONFIG_NEW_DS_EMPTY_STATE:"config_new_ds_empty_state",SEARCH:"search",DISMISS:"dismiss"};function ye({tracing:e,dashboard:a,mixed:n,metrics:s,type:r,annotations:o,variables:c,alerting:u,pluginId:f,logs:g,uploadFile:v,filter:y,onChange:m,current:h,onDismiss:S,reportedInteractionFrom:E}){const p=(0,O.wW)(et),[X,U]=(0,t.useState)(""),T=E||"modal",R=()=>{S(),(0,w.ff)($,{item:W.DISMISS,src:T})},q=D=>{m(D),(0,w.ff)($,{item:W.SELECT_DS,ds_type:D.type,src:T})},L=t.useMemo(()=>(0,Pe.once)(()=>{(0,w.ff)($,{item:"search",src:T})}),[T]),_=oe("-- Grafana --"),le=(0,Me.k)((D,ue)=>{_&&(m(_,[D]),(0,w.ff)($,{item:W.UPLOAD_FILE,src:T}),ue.length<1&&S())}),z=({className:D})=>t.createElement(Ze,{className:D,onChange:q,current:h,filter:y,variables:c,tracing:e,metrics:s,type:r,annotations:o,alerting:u,pluginId:f,logs:g,dashboard:a,mixed:n});return t.createElement(M.u,{title:"Select data source",closeOnEscape:!0,closeOnBackdropClick:!0,isOpen:!0,className:p.modal,contentClassName:p.modalContent,onClickBackdrop:R,onDismiss:R},t.createElement("div",{className:p.leftColumn},t.createElement(A.I,{type:"search",autoFocus:!0,className:p.searchInput,value:X,prefix:t.createElement(re.J,{name:"search"}),placeholder:"Search data source",onChange:D=>{U(D.currentTarget.value),L()}}),t.createElement(I.$,null,t.createElement(Ee,{onChange:q,current:h,onClickEmptyStateCTA:()=>(0,w.ff)($,{item:W.CONFIG_NEW_DS_EMPTY_STATE,src:T}),filter:D=>(y?y?.(D):!0)&&Se(D,X)&&!D.meta.builtIn,variables:c,tracing:e,metrics:s,type:r,annotations:o,alerting:u,pluginId:f,logs:g,dashboard:a,mixed:n}),t.createElement(z,{className:p.appendBuiltInDataSourcesList}))),t.createElement("div",{className:p.rightColumn},t.createElement("div",{className:p.builtInDataSources},t.createElement(I.$,{className:p.builtInDataSourcesList},t.createElement(z,null)),v&&ke.v.featureToggles.editPanelCSVDragAndDrop&&t.createElement(he.Yo,{readAs:"readAsArrayBuffer",fileListRenderer:()=>{},options:{maxSize:fe.dg,multiple:!1,accept:fe.DK,onDrop:le}},t.createElement(he.A_,null))),t.createElement("div",{className:p.newDSSection},t.createElement("span",{className:p.newDSDescription},"Open a new tab and configure a data source"),t.createElement(ge,{variant:"secondary",onClick:()=>{(0,w.ff)($,{item:W.CONFIG_NEW_DS,src:T}),S()}}))))}function et(e){return{modal:l.css`
      width: 80%;
      height: 80%;
      max-width: 1200px;
      max-height: 900px;

      ${e.breakpoints.down("md")} {
        width: 100%;
      }
    `,modalContent:l.css`
      display: flex;
      flex-direction: row;
      height: 100%;

      ${e.breakpoints.down("md")} {
        flex-direction: column;
      }
    `,leftColumn:l.css`
      display: flex;
      flex-direction: column;
      width: 50%;
      height: 100%;
      padding-right: ${e.spacing(4)};
      border-right: 1px solid ${e.colors.border.weak};

      ${e.breakpoints.down("md")} {
        width: 100%;
        border-right: 0;
        padding-right: 0;
        flex: 1;
        overflow-y: auto;
      }
    `,rightColumn:l.css`
      display: flex;
      flex-direction: column;
      width: 50%;
      height: 100%;
      justify-items: space-evenly;
      align-items: stretch;
      padding-left: ${e.spacing(4)};

      ${e.breakpoints.down("md")} {
        width: 100%;
        padding-left: 0;
        flex: 0;
      }
    `,builtInDataSources:l.css`
      flex: 1 1;
      margin-bottom: ${e.spacing(4)};

      ${e.breakpoints.down("md")} {
        flex: 0;
      }
    `,builtInDataSourcesList:l.css`
      ${e.breakpoints.down("md")} {
        display: none;
        margin-bottom: 0;
      }

      margin-bottom: ${e.spacing(4)};
    `,appendBuiltInDataSourcesList:l.css`
      ${e.breakpoints.up("md")} {
        display: none;
      }
    `,newDSSection:l.css`
      display: flex;
      flex-direction: row;
      width: 100%;
      justify-content: space-between;
      align-items: center;
    `,newDSDescription:l.css`
      flex: 1 0;
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      color: ${e.colors.text.secondary};
    `,searchInput:l.css`
      width: 100%;
      min-height: 32px;
      margin-bottom: ${e.spacing(1)};
    `}}var tt=i(53117),at=i(29952),nt=i(86922),st=i(10092),rt=i(85511),ot=i(5191),it=i(54350),ct=i(7832);function lt(e){const{dataSource:a}=e,n=(0,O.wW)(xe);return a?t.createElement("img",{className:n.pickerDSLogo,alt:`${a.meta.name} logo`,src:a.meta.info.logos.small}):De()}function De(){const e=(0,O.wW)(xe);return t.createElement("div",{className:e.pickerDSLogo})}function xe(e){return{pickerDSLogo:l.css`
      height: 20px;
      width: 20px;
    `}}var ut=i(85057);const dt=20,mt=200,ht={name:"maxSize",enabled:!0,phase:"main",requires:["offset","preventOverflow","flip"],fn({state:e,name:a,options:n}){const s=(0,ut.Z)(e,n),{x:r,y:o}=e.modifiersData.preventOverflow||{x:0,y:0},{width:c,height:u}=e.rects.popper,[f]=e.placement.split("-"),g=f==="left"?"left":"right",v=f==="top"?"top":"bottom";e.modifiersData[a]={width:c-s[g]-r,height:u-s[v]-o}}},ft={name:"applyMaxSize",enabled:!0,phase:"beforeWrite",requires:["maxSize"],fn({state:e}){const{height:a,width:n}=e.modifiersData.maxSize;e.styles.popper.maxHeight||(e.styles.popper.maxHeight=`${a-dt}px`),e.styles.popper.minHeight||(e.styles.popper.minHeight=`${mt}px`),e.styles.popper.maxWidth||(e.styles.popper.maxWidth=n)}},H="dashboards_dspicker_clicked",V={OPEN_DROPDOWN:"open_dspicker",SELECT_DS:"select_ds",ADD_FILE:"add_file",OPEN_ADVANCED_DS_PICKER:"open_advanced_ds_picker",CONFIG_NEW_DS_EMPTY_STATE:"config_new_ds_empty_state"};function pt(e){const{current:a,onChange:n,hideTextValue:s=!1,width:r,inputId:o,noDefault:c=!1,disabled:u=!1,placeholder:f="Select data source",...g}=e,[v,y]=(0,t.useState)(!1),[m,h]=(0,t.useState)(!1),[S,E]=(0,t.useState)(),[p,X]=(0,t.useState)(),[U,T]=(0,t.useState)(""),R=()=>{(0,w.ff)(H,{item:V.OPEN_DROPDOWN}),y(!0),S?.focus()},q=oe(a),L=Boolean(!a&&c)?void 0:q,_=U&&v?t.createElement(De,null):t.createElement(lt,{dataSource:L}),{onKeyDown:le,keyboardEvents:z}=(0,it.A)();(0,t.useEffect)(()=>{const N=z.subscribe({next:F=>{switch(F?.code){case"ArrowDown":{R(),F.preventDefault();break}case"ArrowUp":R(),F.preventDefault();break;case"Escape":Q(),S?.focus(),F.preventDefault()}}});return()=>N.unsubscribe()});const D=oe("-- Grafana --"),ue=()=>{D&&n(D,[ct.ip])},Ne=(0,st.D)(S,p,{placement:"bottom-start",modifiers:[{name:"offset",options:{offset:[0,4]}},ht,ft]}),Q=(0,t.useCallback)(()=>{T(""),y(!1)},[y]),de=(0,t.useRef)(null),{overlayProps:Rt,underlayProps:Pt}=(0,nt.Ir)({onClose:Q,isDismissable:!0,isOpen:v,shouldCloseOnInteractOutside:N=>S?!S.isSameNode(N):!1},de),{dialogProps:kt}=(0,at.R)({},de),me=(0,O.wW)(N=>gt(N,e));return t.createElement("div",{className:me.container,"data-testid":P.wl.components.DataSourcePicker.container},t.createElement("div",{className:me.trigger,onClick:R},t.createElement(A.I,{id:o||"data-source-picker",className:m?void 0:me.input,"data-testid":P.wl.components.DataSourcePicker.inputV2,prefix:L?_:void 0,suffix:t.createElement(re.J,{name:v?"search":"angle-down"}),placeholder:s?"":Ke(L)||f,onClick:R,onFocus:()=>{h(!0)},onBlur:()=>{h(!1),Q()},onKeyDown:le,value:U,onChange:N=>{R(),T(N.currentTarget.value)},ref:E,disabled:u})),v?t.createElement(rt.h_,null,t.createElement("div",{...Pt}),t.createElement("div",{ref:de,...Rt,...kt,onMouseDown:N=>{N.preventDefault()}},t.createElement(Ce,{keyboardEvents:z,filterTerm:U,onChange:(N,F)=>{Q(),n(N,F)},onClose:Q,current:L,style:Ne.styles.popper,ref:X,onClickAddCSV:ue,...g,onDismiss:Q,...Ne.attributes.popper}))):null)}function gt(e,a){return{container:l.css`
      position: relative;
      cursor: ${a.disabled?"not-allowed":"pointer"};
      width: ${e.spacing(a.width||"auto")};
    `,trigger:l.css`
      cursor: pointer;
      ${a.disabled&&"pointer-events: none;"}
    `,input:l.css`
      input::placeholder {
        color: ${a.disabled?e.colors.action.disabledText:e.colors.text.primary};
      }
    `}}const Ce=t.forwardRef((e,a)=>{const{filterTerm:n,onChange:s,onClose:r,onClickAddCSV:o,current:c,filter:u,uploadFile:f}=e,g=(0,t.useCallback)(m=>{s(m),(0,w.ff)(H,{item:V.SELECT_DS,ds_type:m.type})},[s]),v=(0,t.useCallback)(()=>{o?.(),r(),(0,w.ff)(H,{item:V.ADD_FILE})},[o,r]),y=(0,O.wW)(vt);return t.createElement("div",{style:e.style,ref:a,className:y.container},t.createElement(I.$,null,t.createElement(Ee,{...e,enableKeyboardNavigation:!0,className:y.dataSourceList,current:c,onChange:g,filter:m=>(u?u?.(m):!0)&&Se(m,n),onClickEmptyStateCTA:()=>(0,w.ff)(H,{item:V.CONFIG_NEW_DS_EMPTY_STATE})})),t.createElement("div",{className:y.footer},t.createElement(ot.JY,null,({showModal:m,hideModal:h})=>t.createElement(d.zx,{size:"sm",variant:"secondary",fill:"text",onClick:()=>{r(),m(ye,{reportedInteractionFrom:"ds_picker",tracing:e.tracing,dashboard:e.dashboard,mixed:e.mixed,metrics:e.metrics,type:e.type,annotations:e.annotations,variables:e.variables,alerting:e.alerting,pluginId:e.pluginId,logs:e.logs,filter:e.filter,uploadFile:e.uploadFile,current:e.current,onDismiss:h,onChange:(S,E)=>{s(S,E),h()}}),(0,w.ff)(H,{item:V.OPEN_ADVANCED_DS_PICKER})}},"Open advanced data source picker",t.createElement(re.J,{name:"arrow-right"}))),f&&k.ZP.featureToggles.editPanelCSVDragAndDrop&&t.createElement(d.zx,{variant:"secondary",size:"sm",onClick:v},"Add csv or spreadsheet")))});Ce.displayName="PickerContent";function vt(e){return{container:l.css`
      display: flex;
      flex-direction: column;
      max-width: 480px;
      background: ${e.colors.background.primary};
      box-shadow: ${e.shadows.z3};
    `,picker:l.css`
      background: ${e.colors.background.secondary};
    `,dataSourceList:l.css`
      flex: 1;
    `,footer:l.css`
      flex: 0;
      display: flex;
      flex-direction: row-reverse;
      justify-content: space-between;
      padding: ${e.spacing(1.5)};
      border-top: 1px solid ${e.colors.border.weak};
      background-color: ${e.colors.background.secondary};
    `}}function be(e){return k.vc.featureToggles.advancedDataSourcePicker?t.createElement(pt,{...e}):t.createElement(tt.q,{...e})}var St=i(47471),ce=i(79543),Ie=i(89749),Et=i(22069);async function yt(e,a,n,s){let r=n;const o={type:e.type,uid:a},c={...e?.getDefaultQuery?.(B.zj.PanelEditor),datasource:o,refId:"A"};if(s?.meta.id!==e.meta.id){if(e.meta.mixed)return n;if((0,Ie.p)(s)&&(0,Ie.CZ)(e)){const u=await s.exportToAbstractQueries(n);r=await e.importFromAbstractQueries(u)}else if(s&&e.importQueries)r=await e.importQueries(n,s);else return[c]}return r.length===0?[c]:r.map(u=>(!(0,Et.Pr)(u.datasource)&&!e.meta.mixed&&(u.datasource=o),u))}var Dt=i(9386),xt=i(84457),Ct=i(47891),bt=i(81764),It=i(8944),wt=i(67254);class Nt extends t.PureComponent{constructor(a){super(a),this.onRelativeTimeChange=s=>{this.setState({timeRangeFrom:s.target.value})},this.onTimeShiftChange=s=>{this.setState({timeRangeShift:s.target.value})},this.onOverrideTime=s=>{const{options:r,onChange:o}=this.props,c=J(s.target.value),u=we(c);u&&r.timeRange?.from!==c&&o({...r,timeRange:{...r.timeRange??{},from:c}}),this.setState({relativeTimeIsValid:u})},this.onTimeShift=s=>{const{options:r,onChange:o}=this.props,c=J(s.target.value),u=we(c);u&&r.timeRange?.shift!==c&&o({...r,timeRange:{...r.timeRange??{},shift:c}}),this.setState({timeShiftIsValid:u})},this.onToggleTimeOverride=()=>{const{onChange:s,options:r}=this.props;this.setState({timeRangeHide:!this.state.timeRangeHide},()=>{s({...r,timeRange:{...r.timeRange??{},hide:this.state.timeRangeHide}})})},this.onCacheTimeoutBlur=s=>{const{options:r,onChange:o}=this.props;o({...r,cacheTimeout:J(s.target.value)})},this.onQueryCachingTTLBlur=s=>{const{options:r,onChange:o}=this.props;let c=parseInt(s.target.value,10);(isNaN(c)||c===0)&&(c=null),o({...r,queryCachingTTL:c})},this.onMaxDataPointsBlur=s=>{const{options:r,onChange:o}=this.props;let c=parseInt(s.target.value,10);(isNaN(c)||c===0)&&(c=null),c!==r.maxDataPoints&&o({...r,maxDataPoints:c})},this.onMinIntervalBlur=s=>{const{options:r,onChange:o}=this.props,c=J(s.target.value);c!==r.minInterval&&o({...r,minInterval:c})},this.onOpenOptions=()=>{this.setState({isOpen:!0})},this.onCloseOptions=()=>{this.setState({isOpen:!1})};const{options:n}=a;this.state={timeRangeFrom:n.timeRange?.from||"",timeRangeShift:n.timeRange?.shift||"",timeRangeHide:n.timeRange?.hide??!1,isOpen:!1,relativeTimeIsValid:!0,timeShiftIsValid:!0}}renderCacheTimeoutOption(){const{dataSource:a,options:n}=this.props,s=`If your time series store has a query cache this option can override the default cache timeout. Specify a
    numeric value in seconds.`;return a.meta.queryOptions?.cacheTimeout?t.createElement("div",{className:"gf-form-inline"},t.createElement("div",{className:"gf-form"},t.createElement(b.c,{width:9,tooltip:s},"Cache timeout"),t.createElement(A.I,{type:"text",className:"width-6",placeholder:"60",spellCheck:!1,onBlur:this.onCacheTimeoutBlur,defaultValue:n.cacheTimeout??""}))):null}renderQueryCachingTTLOption(){const{dataSource:a,options:n}=this.props,s="Cache time-to-live: How long results from this queries in this panel will be cached, in milliseconds. Defaults to the TTL in the caching configuration for this datasource.";return a.cachingConfig?.enabled?t.createElement("div",{className:"gf-form-inline"},t.createElement("div",{className:"gf-form"},t.createElement(b.c,{width:9,tooltip:s},"Cache TTL"),t.createElement(A.I,{type:"number",className:"width-6",placeholder:`${a.cachingConfig.TTLMs}`,spellCheck:!1,onBlur:this.onQueryCachingTTLBlur,defaultValue:n.queryCachingTTL??void 0}))):null}renderMaxDataPointsOption(){const{data:a,options:n}=this.props,s=a.request?.maxDataPoints,r=n.maxDataPoints??"",o=r==="";return t.createElement("div",{className:"gf-form-inline"},t.createElement("div",{className:"gf-form"},t.createElement(b.c,{width:9,tooltip:t.createElement(t.Fragment,null,"The maximum data points per series. Used directly by some data sources and used in calculation of auto interval. With streaming data this value is used for the rolling buffer.")},"Max data points"),t.createElement(A.I,{type:"number",className:"width-6",placeholder:`${s}`,spellCheck:!1,onBlur:this.onMaxDataPointsBlur,defaultValue:r}),o&&t.createElement(t.Fragment,null,t.createElement("div",{className:"gf-form-label query-segment-operator"},"="),t.createElement("div",{className:"gf-form-label"},"Width of panel"))))}renderIntervalOption(){const{data:a,dataSource:n,options:s}=this.props,r=a.request?.interval,o=n.interval??"No limit";return t.createElement(t.Fragment,null,t.createElement("div",{className:"gf-form-inline"},t.createElement("div",{className:"gf-form"},t.createElement(b.c,{width:9,tooltip:t.createElement(t.Fragment,null,"A lower limit for the interval. Recommended to be set to write frequency, for example ",t.createElement("code",null,"1m")," ","if your data is written every minute. Default value can be set in data source settings for most data sources.")},"Min interval"),t.createElement(A.I,{type:"text",className:"width-6",placeholder:`${o}`,spellCheck:!1,onBlur:this.onMinIntervalBlur,defaultValue:s.minInterval??""}))),t.createElement("div",{className:"gf-form-inline"},t.createElement("div",{className:"gf-form"},t.createElement(b.c,{width:9,tooltip:t.createElement(t.Fragment,null,"The evaluated interval that is sent to data source and is used in ",t.createElement("code",null,"$__interval")," and"," ",t.createElement("code",null,"$__interval_ms"))},"Interval"),t.createElement(b.c,{width:6},r),t.createElement("div",{className:"gf-form-label query-segment-operator"},"="),t.createElement("div",{className:"gf-form-label"},"Time range / max data points"))))}renderCollapsedText(a){const{data:n,options:s}=this.props,{isOpen:r}=this.state;if(r)return;let o=s.maxDataPoints??"";o===""&&n.request&&(o=`auto = ${n.request.maxDataPoints}`);let c=s.minInterval;return n.request&&(c=`${n.request.interval}`),t.createElement(t.Fragment,null,t.createElement("div",{className:a.collapsedText},"MD = ",o),t.createElement("div",{className:a.collapsedText},"Interval = ",c))}render(){const{timeRangeHide:a,relativeTimeIsValid:n,timeShiftIsValid:s}=this.state,{timeRangeFrom:r,timeRangeShift:o,isOpen:c}=this.state,u=Tt();return t.createElement(wt.t,{id:"Query options",index:0,title:"Query options",headerElement:this.renderCollapsedText(u),isOpen:c,onOpen:this.onOpenOptions,onClose:this.onCloseOptions},this.renderMaxDataPointsOption(),this.renderIntervalOption(),this.renderCacheTimeoutOption(),this.renderQueryCachingTTLOption(),t.createElement("div",{className:"gf-form"},t.createElement(b.c,{width:9},"Relative time"),t.createElement(A.I,{type:"text",className:"width-6",placeholder:"1h",onChange:this.onRelativeTimeChange,onBlur:this.onOverrideTime,invalid:!n,value:r})),t.createElement("div",{className:"gf-form"},t.createElement("span",{className:"gf-form-label width-9"},"Time shift"),t.createElement(A.I,{type:"text",className:"width-6",placeholder:"1h",onChange:this.onTimeShiftChange,onBlur:this.onTimeShift,invalid:!s,value:o})),(o||r)&&t.createElement("div",{className:"gf-form-inline"},t.createElement(bt._,{label:"Hide time info",labelWidth:18},t.createElement(It.r,{value:a,onChange:this.onToggleTimeOverride}))))}}const we=e=>e?Ct.isValidTimeSpan(e):!0,J=e=>e===""?null:e,Tt=(0,K.B)(()=>{const{theme:e}=k.vc;return{collapsedText:l.css`
      margin-left: ${e.spacing.md};
      font-size: ${e.typography.size.sm};
      color: ${e.colors.textWeak};
    `}});class Ot extends t.PureComponent{constructor(){super(...arguments),this.backendSrv=Re.ae,this.dataSourceSrv=(0,x.F)(),this.querySubscription=null,this.state={isDataSourceModalOpen:!!C.E1.getSearchObject().firstPanel,isLoadingHelp:!1,helpContent:null,isPickerOpen:!1,isAddingMixed:!1,isHelpOpen:!1,queries:[],data:{state:te.Gu.NotStarted,series:[],timeRange:(0,ae.JK)()}},this.onChangeDataSource=async(a,n)=>{const{dsSettings:s}=this.state,r=s?await(0,x.F)().get(s.uid):void 0,o=await(0,x.F)().get(a.uid),c=n||await yt(o,a.uid,this.state.queries,r),u=await this.dataSourceSrv.get(a.name);this.onChange({queries:c,dataSource:{name:a.name,uid:a.uid,type:a.meta.id,default:a.isDefault}}),this.setState({queries:c,dataSource:u,dsSettings:a}),n&&this.props.onRunQueries()},this.onAddQueryClick=()=>{const{queries:a}=this.state;this.onQueriesChange((0,Z.DI)(a,this.newQuery())),this.onScrollBottom()},this.onAddExpressionClick=()=>{this.onQueriesChange((0,Z.DI)(this.state.queries,St.mV.newQuery())),this.onScrollBottom()},this.onScrollBottom=()=>{setTimeout(()=>{this.state.scrollElement&&this.state.scrollElement.scrollTo({top:1e4})},20)},this.onUpdateAndRun=a=>{this.props.onOptionsChange(a),this.props.onRunQueries()},this.onOpenHelp=()=>{this.setState({isHelpOpen:!0})},this.onCloseHelp=()=>{this.setState({isHelpOpen:!1})},this.onCloseDataSourceModal=()=>{this.setState({isDataSourceModalOpen:!1})},this.renderMixedPicker=()=>t.createElement(be,{mixed:!1,onChange:this.onAddMixedQuery,current:null,autoFocus:!0,variables:!0,onBlur:this.onMixedPickerBlur,openMenuOnFocus:!0}),this.renderDataSourcePickerWithPrompt=()=>{const{isDataSourceModalOpen:a}=this.state,n={metrics:!0,mixed:!0,dashboard:!0,variables:!0,current:this.props.options.dataSource,uploadFile:!0,onChange:async(s,r)=>{await this.onChangeDataSource(s,r),this.onCloseDataSourceModal()}};return t.createElement(t.Fragment,null,a&&k.ZP.featureToggles.advancedDataSourcePicker&&t.createElement(ye,{...n,onDismiss:this.onCloseDataSourceModal}),t.createElement(be,{...n}))},this.onAddMixedQuery=a=>{this.onAddQuery({datasource:a.name}),this.setState({isAddingMixed:!1})},this.onMixedPickerBlur=()=>{this.setState({isAddingMixed:!1})},this.onAddQuery=a=>{const{dsSettings:n,queries:s}=this.state;this.onQueriesChange((0,Z.DI)(s,a,{type:n?.type,uid:n?.uid})),this.onScrollBottom()},this.onQueriesChange=a=>{this.onChange({queries:a}),this.setState({queries:a})},this.setScrollRef=a=>{this.setState({scrollElement:a})}}async componentDidMount(){const{options:a,queryRunner:n}=this.props;this.querySubscription=n.getData({withTransforms:!1,withFieldConfig:!1}).subscribe({next:s=>this.onPanelDataUpdate(s)}),this.setNewQueriesAndDatasource(a),C.E1.getSearchObject().firstPanel&&C.E1.partial({firstPanel:null},!0)}componentWillUnmount(){this.querySubscription&&(this.querySubscription.unsubscribe(),this.querySubscription=null)}async componentDidUpdate(){const{options:a}=this.props,n=await(0,x.F)().get(a.dataSource);this.state.dataSource&&n.uid!==this.state.dataSource?.uid&&this.setNewQueriesAndDatasource(a)}async setNewQueriesAndDatasource(a){try{const n=await this.dataSourceSrv.get(a.dataSource),s=this.dataSourceSrv.getInstanceSettings(a.dataSource),r=await this.dataSourceSrv.get(),o=n.getRef(),c=a.queries.map(u=>({...(0,Z.vH)(u)&&n?.getDefaultQuery?.(B.zj.PanelEditor),datasource:o,...u}));this.setState({queries:c,dataSource:n,dsSettings:s,defaultDataSource:r})}catch(n){console.log("failed to load data source",n)}}onPanelDataUpdate(a){this.setState({data:a})}newQuery(){const{dsSettings:a,defaultDataSource:n}=this.state,s=a?.meta.mixed?n:a;return{...this.state.dataSource?.getDefaultQuery?.(B.zj.PanelEditor),datasource:{uid:s?.uid,type:s?.type}}}onChange(a){this.props.onOptionsChange({...this.props.options,...a})}renderTopSection(a){const{onOpenQueryInspector:n,options:s}=this.props,{dataSource:r,data:o}=this.state;return t.createElement("div",null,t.createElement("div",{className:a.dataSourceRow},t.createElement(b.c,{htmlFor:"data-source-picker",width:"auto"},"Data source"),t.createElement("div",{className:a.dataSourceRowItem},this.renderDataSourcePickerWithPrompt()),r&&t.createElement(t.Fragment,null,t.createElement("div",{className:a.dataSourceRowItem},t.createElement(d.zx,{variant:"secondary",icon:"question-circle",title:"Open data source help",onClick:this.onOpenHelp,"data-testid":"query-tab-help-button"})),t.createElement("div",{className:a.dataSourceRowItemOptions},t.createElement(Nt,{options:s,dataSource:r,data:o,onChange:this.onUpdateAndRun})),n&&t.createElement("div",{className:a.dataSourceRowItem},t.createElement(d.zx,{variant:"secondary",onClick:n,"aria-label":P.wl.components.QueryTab.queryInspectorButton},"Query inspector")))))}renderQueries(a){const{onRunQueries:n}=this.props,{data:s,queries:r}=this.state;return(0,ce.yl)(a.name)?t.createElement(ce.hD,{queries:r,panelData:s,onChange:this.onQueriesChange,onRunQueries:n}):t.createElement("div",{"aria-label":P.wl.components.QueryTab.content},t.createElement(xt.l,{queries:r,dsSettings:a,onQueriesChange:this.onQueriesChange,onAddQuery:this.onAddQuery,onRunQueries:n,data:s}))}isExpressionsSupported(a){return(a.meta.alerting||a.meta.mixed)===!0}renderExtraActions(){return Dt.S.getAllExtraRenderAction().map((a,n)=>a({onAddQuery:this.onAddQuery,onChangeDataSource:this.onChangeDataSource,key:n})).filter(Boolean)}renderAddQueryRow(a,n){const{isAddingMixed:s}=this.state,r=!(s||(0,ce.yl)(a.name));return t.createElement(ne.Lh,{spacing:"md",align:"flex-start"},r&&t.createElement(d.zx,{icon:"plus",onClick:this.onAddQueryClick,variant:"secondary","aria-label":P.wl.components.QueryTab.addQuery,"data-testid":"query-tab-add-query"},"Query"),k.ZP.expressionsEnabled&&this.isExpressionsSupported(a)&&t.createElement(d.zx,{icon:"plus",onClick:this.onAddExpressionClick,variant:"secondary",className:n.expressionButton,"data-testid":"query-tab-add-expression"},t.createElement("span",null,"Expression\xA0")),this.renderExtraActions())}render(){const{isHelpOpen:a,dsSettings:n}=this.state,s=At();return t.createElement(I.$,{autoHeightMin:"100%",scrollRefCallback:this.setScrollRef},t.createElement("div",{className:s.innerWrapper},this.renderTopSection(s),n&&t.createElement(t.Fragment,null,t.createElement("div",{className:s.queriesWrapper},this.renderQueries(n)),this.renderAddQueryRow(n,s),a&&t.createElement(M.u,{title:"Data source help",isOpen:!0,onDismiss:this.onCloseHelp},t.createElement(Ae,{pluginId:n.meta.id})))))}}const At=(0,K.B)(()=>{const{theme:e}=k.ZP;return{innerWrapper:l.css`
      display: flex;
      flex-direction: column;
      padding: ${e.spacing.md};
    `,dataSourceRow:l.css`
      display: flex;
      margin-bottom: ${e.spacing.md};
    `,dataSourceRowItem:l.css`
      margin-right: ${e.spacing.inlineFormMargin};
    `,dataSourceRowItemOptions:l.css`
      flex-grow: 1;
      margin-right: ${e.spacing.inlineFormMargin};
    `,queriesWrapper:l.css`
      padding-bottom: 16px;
    `,expressionWrapper:l.css``,expressionButton:l.css`
      margin-right: ${e.spacing.sm};
    `}})},54350:(Te,ee,i)=>{i.d(ee,{A:()=>B,v:()=>P});var l=i(66111),t=i(91017),te=i(72196),ae=i(37932);function B(){const x=(0,l.useRef)(new t.x);return{keyboardEvents:x.current,onKeyDown:C=>{switch(C.code){case"ArrowDown":case"ArrowUp":case"ArrowLeft":case"ArrowRight":case"Enter":x.current.next(C);default:}}}}function P(x,C,b){const d=(0,l.useRef)({x:0,y:-1}),[ne,I]=(0,l.useState)({x:0,y:-1}),M=(0,l.useRef)();return(0,l.useEffect)(()=>{M.current=b.view.fields.url,d.current.x=0,d.current.y=-1,I({...d.current})},[b]),(0,l.useEffect)(()=>{const K=x.subscribe({next:se=>{switch(se?.code){case"ArrowDown":{d.current.y++,I({...d.current});break}case"ArrowUp":d.current.y=Math.max(0,d.current.y-1),I({...d.current});break;case"ArrowRight":{C>0&&(d.current.x=Math.min(C,d.current.x+1),I({...d.current}));break}case"ArrowLeft":{C>0&&(d.current.x=Math.max(0,d.current.x-1),I({...d.current}));break}case"Enter":if(!M.current)break;const G=d.current.x*C+d.current.y;if(G<0){d.current.x=0,d.current.y=0,I({...d.current});break}const j=M.current.values?.[G];j&&ae.E1.push(te.u.stripBaseFromUrl(j))}}});return()=>K.unsubscribe()},[x,C]),ne}}}]);

//# sourceMappingURL=1579.e4289c286deae70fcd67.js.map