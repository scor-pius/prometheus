"use strict";(self.webpackChunkgrafana=self.webpackChunkgrafana||[]).push([[1333],{39597:(o,l,n)=>{n.r(l),n.d(l,{default:()=>r});var e=n(66111),a=n(8412),t=n(76770);function r(){const u=(0,t.q)("alert-list");return e.createElement(a.T,{navModel:u},e.createElement(a.T.Contents,null,e.createElement("h1",null,"Alerting is not enabled"),"To enable alerting, enable it in the Grafana config:",e.createElement("div",null,e.createElement("pre",null,`[unified_alerting]
enable = true
`)),e.createElement("div",null,"For legacy alerting",e.createElement("pre",null,`[alerting]
enable = true
`))))}}}]);

//# sourceMappingURL=AlertingFeatureTogglePage.4679c599a87ca8d7c2a6.js.map