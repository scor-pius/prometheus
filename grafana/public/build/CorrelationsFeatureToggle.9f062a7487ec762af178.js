"use strict";(self.webpackChunkgrafana=self.webpackChunkgrafana||[]).push([[2093],{91598:(r,n,a)=>{a.r(n),a.d(n,{default:()=>l});var e=a(66111),t=a(8412);function l(){return e.createElement(t.T,{navId:"correlations"},e.createElement(t.T.Contents,null,e.createElement("h1",null,"Correlations are disabled"),"To enable Correlations, add it in the Grafana config:",e.createElement("div",null,e.createElement("pre",null,`[feature_toggles]
correlations = true
`))))}}}]);

//# sourceMappingURL=CorrelationsFeatureToggle.9f062a7487ec762af178.js.map