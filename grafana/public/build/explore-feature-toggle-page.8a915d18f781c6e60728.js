"use strict";(self.webpackChunkgrafana=self.webpackChunkgrafana||[]).push([[172],{20750:(c,n,e)=>{e.r(n),e.d(n,{default:()=>E});var t=e(9892),a=e(66111),s=e(72648),l=e(8412);function E(){const o=(0,s.wW)(r=>t.css`
        margin-top: ${r.spacing(2)};
      `);return a.createElement(l.T,{className:o},a.createElement(l.T.Contents,null,a.createElement("h1",null,"Explore is disabled"),"To enable Explore, enable it in the Grafana config:",a.createElement("div",null,a.createElement("pre",null,`[explore]
enable = true
`))))}}}]);

//# sourceMappingURL=explore-feature-toggle-page.8a915d18f781c6e60728.js.map